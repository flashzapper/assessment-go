# Assignment Go Project

## Requirements
- go 1.19
- Docker
- docker-compose
- [GNU Make](https://askubuntu.com/questions/161104/how-do-i-install-make)
- [Golang Migrate](https://github.com/golang-migrate/migrate)



## How to start dev
- execute `make start-dev` to run database container
- execute `make start-kafka` to run kafka container
- execute `make run-account-server` to run `account` HTTP service
- execute `make run-mutation-server` to run `mutation` consumer service

## Note after start dev
Please execute this if you running the service for the first time : 

- `make create-database`
- `make migrate`