CREATE TABLE IF NOT EXISTS assessment.mutations(
    uuid uuid NOT NULL DEFAULT uuid_generate_v4(),
    account_number varchar(255) NOT NULL,
    code varchar(2) NOT NULL,
    amount float NOT NULL,
    created_time timestamptz NOT NULL DEFAULT now(),
    updated_time timestamptz NULL,
    is_removed boolean NOT NULL DEFAULT false,
    removed_time timestamptz NULL,
    CONSTRAINT assessment_mutations__uuid PRIMARY KEY (uuid),
    CONSTRAINT assessment_mutations__account_number FOREIGN KEY(account_number) REFERENCES assessment.users(account_number) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE INDEX IF NOT EXISTS assessment_mutations__account_number__index
    ON assessment.mutations USING btree
    (account_number ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS assessment_mutations__created_date__index
    ON assessment.mutations USING btree
    (created_time DESC NULLS LAST)
    TABLESPACE pg_default;