CREATE TABLE IF NOT EXISTS assessment.accounts(
    uuid uuid NOT NULL DEFAULT uuid_generate_v4(),
    account_number varchar(255) NOT NULL,
    balance float NOT NULL DEFAULT 0,
    status smallint NOT NULL DEFAULT 1,
    created_time timestamptz NOT NULL DEFAULT now(),
    updated_time timestamptz NULL,
    is_removed boolean NOT NULL DEFAULT false,
    removed_time timestamptz NULL,
    CONSTRAINT assessment_accounts__uuid PRIMARY KEY (uuid),
    CONSTRAINT assessment_accounts__account_number FOREIGN KEY(account_number) REFERENCES assessment.users(account_number) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE INDEX IF NOT EXISTS assessment_accounts__account_number__index
    ON assessment.mutations USING btree
    (account_number ASC NULLS LAST)
    TABLESPACE pg_default;
