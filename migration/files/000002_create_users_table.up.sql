CREATE TABLE IF NOT EXISTS assessment.users(
    uuid uuid NOT NULL DEFAULT uuid_generate_v4(),
    name varchar(255) NOT NULL,
    nik varchar(255) NOT NULL,
    account_number varchar(255) NOT NULL,
    phone_number varchar(255) NOT NULL,
    status smallint NOT NULL DEFAULT 1,
    created_time timestamptz NOT NULL DEFAULT now(),
    updated_time timestamptz NULL,
    is_removed boolean NOT NULL DEFAULT false,
    removed_time timestamptz NULL,
    CONSTRAINT assessment_users__uuid PRIMARY KEY (uuid),
    CONSTRAINT assessment_users__phone_number UNIQUE (phone_number),
    CONSTRAINT assessment_users__nik UNIQUE (nik),
    CONSTRAINT assessment_users__account_number UNIQUE (account_number)
);