module assessment-go

go 1.19

require (
	github.com/go-pg/pg/v10 v10.11.1
	github.com/gofrs/uuid v4.4.0+incompatible
	github.com/labstack/echo/v4 v4.11.1
	gopkg.in/ini.v1 v1.67.0
)

require (
	github.com/gabriel-vasile/mimetype v1.4.2 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/leodido/go-urn v1.2.4 // indirect
	golang.org/x/time v0.3.0 // indirect
)

require (
	github.com/confluentinc/confluent-kafka-go v1.9.2
	github.com/go-pg/zerochecker v0.2.0 // indirect
	github.com/go-playground/validator/v10 v10.15.5
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/labstack/gommon v0.4.0
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/tmthrgd/go-hex v0.0.0-20190904060850-447a3041c3bc // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	github.com/vmihailenco/bufpool v0.1.11 // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.4 // indirect
	github.com/vmihailenco/tagparser v0.1.2 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/crypto v0.11.0 // indirect
	golang.org/x/net v0.12.0 // indirect
	golang.org/x/sys v0.10.0 // indirect
	golang.org/x/text v0.11.0 // indirect
	mellium.im/sasl v0.3.1 // indirect
)
