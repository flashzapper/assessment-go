package main

import (
	handler "assessment-go/internal/service"
)

func main() {
	err := handler.RunServer()
	if err != nil {
		panic(err)
	}
}
