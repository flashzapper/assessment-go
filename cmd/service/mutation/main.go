package main

import (
	k "assessment-go/pkg/modules/kafka"
	"assessment-go/pkg/modules/kafka/model"
	postgres "assessment-go/pkg/repository/postgres/intfc"
	"context"
	"fmt"
	"time"

	pgModel "assessment-go/pkg/model"
	conn "assessment-go/pkg/repository/postgres/connection"
	impl "assessment-go/pkg/repository/postgres/impl"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/go-pg/pg/v10"
	"github.com/labstack/gommon/log"
)

const ConstMaxConcurrentConsumers = 1
const ConstRetryCount = 3

func main() {
	fmt.Print("-- Start Mutation Message Consumer --")

	consumer := k.NewConsumer(nil)
	consumer.SubscribeTopic(string(k.Mutation))

	msgChan := make(chan *kafka.Message, ConstMaxConcurrentConsumers)

	for {
		msg, err := consumer.GetMessage()
		if err == nil {
			msgChan <- msg
			go func(msg2 kafka.Message) {
				defer func() {
					<-msgChan
				}()

				ctx, cancel := context.WithCancel(context.Background())
				defer cancel()

				process(ctx, &msg2, 0)

			}(*msg)
		} else {
			// The client will automatically try to recover from all errors.
			log.Errorf("Consumer error: %v (%v)\n", err, msg)
		}
	}
}

func process(ctx context.Context, msg *kafka.Message, retryCount int) {
	log.Infof("(Retry=%d) Message on %s: %s\n", retryCount, msg.TopicPartition, string(msg.Value))
	var data = model.MutationMessage{}
	data = data.FromKafka(msg.Value)

	defer func() {
		if r := recover(); r != nil && retryCount >= ConstRetryCount {
			log.Errorf("failed insert : %s\n", r)
		} else if r != nil {
			// delay after recover to reyty
			time.Sleep(time.Millisecond * time.Duration(2000))
			retryCount++
			process(ctx, msg, retryCount)
		}
	}()

	err := insertMutation(data)
	if err != nil && retryCount >= ConstRetryCount {
		log.Infof("failed insert : %s\n", err.Error())
	} else if err != nil {
		time.Sleep(time.Millisecond * time.Duration(2000))
		retryCount++
		process(ctx, msg, retryCount)
	} else {
		log.Infof("insert success account number : %s ", data.AccountNumber)
	}
}

func insertMutation(data model.MutationMessage) error {
	var (
		repoWrite postgres.SavingsRepositoryWrite
		trx       conn.Transaction
		result    *pgModel.Mutation
		db        *pg.DB
		err       error
	)

	repoWrite = impl.NewSavingsRepositoryWrite()

	db = conn.GetConnection("write", "assignment")
	if trx, err = conn.NewTransaction(db); err != nil {
		log.Errorf("cannot create transaction. %v", err)
		return err
	}

	defer func() {
		if err := trx.RollbackIfNotCommitted(); err != nil {
			log.Errorf("cannot rollback transaction. %v", err)
		}
	}()

	modelMutation := &pgModel.Mutation{
		AccountNumber: data.AccountNumber,
		Code:          data.Code,
		Amount:        data.Amount,
	}

	result, err = repoWrite.SaveMutation(trx, modelMutation)

	if err != nil {
		return err
	}

	if err = trx.Commit(); err != nil {
		log.Errorf("cannot commit transaction. %v", err)
		return err
	}

	log.Infof("Transaction for account number %s has been committed successfully", result.AccountNumber)

	return err
}
