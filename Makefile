###########################################################################
## Makefile
## @author: Rafli Ahmad Z <flashzapper@gmail.com>
## @since: 2023.10.06
###########################################################################

DOCKER_COMPOSE=docker-compose -f docker-compose.yml -p assessment
DOCKER_COMPOSE_KAFKA=docker-compose -f docker-compose-kafka.yml -p assessment
DATABASE_SOURCE_NAME='postgres://assessment:password@localhost:5432/assessment?sslmode=disable&search_path=public,assessment'

build-image:
	@docker build --platform=linux/amd64 -f cicd/docker/Dockerfile . -t assessment-go:0.0.1

init-dev:
	rm -f .env
	@cp cicd/env/.env .env

start-dev:
	@make init-dev
	$(DOCKER_COMPOSE) up -d

down-dev:
	@$(DOCKER_COMPOSE) down --remove-orphans

start-kafka:
	@$(DOCKER_COMPOSE_KAFKA) up  

create-database:
	$(DOCKER_COMPOSE) exec database createdb -U assessment assessment

migrate:
	@(cd migration; DATABASE_SOURCE_NAME="$(DATABASE_SOURCE_NAME)" make run-migration)

run-account-server:
	@go run cmd/service/account/main.go

run-mutation-server:
	@go run cmd/service/mutation/main.go
	