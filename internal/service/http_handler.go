package handler

import (
	"assessment-go/internal/service/savings/controller"
	"assessment-go/internal/service/savings/helper"
	"assessment-go/pkg/config"
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func RunServer() error {
	config := config.GetEchoServerConfig()

	// Create a new Echo instance
	router := echo.New()

	// Middleware for recovering from panics
	router.Use(middleware.Recover())

	// Initialize validator
	router.Validator = helper.NewValidator()

	// setup routes
	setupRoutes(router)

	// Start the server
	port := fmt.Sprintf(":%s", config.Port)
	return router.Start(port)
}

func setupRoutes(router *echo.Echo) {
	saving := controller.NewSavingsController()

	// Define a route handler for the root path
	router.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Server is online !")
	})

	router.POST("/daftar", saving.Register)
	router.POST("/tabung", saving.Deposit)
	router.POST("/tarik", saving.Withdraw)
	router.GET("/saldo/:no_rekening", saving.Balance)
	router.GET("/mutasi/:no_rekening", saving.Mutation)
}
