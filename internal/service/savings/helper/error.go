package helper

import "net/http"

// Error custom error
type Error struct {
	StatusCode int
	Remark     string
}

func (n *Error) Error() string {
	return n.Remark
}

// NewError create new Error instance
func NewError(remark string, statusCode int) *Error {
	return &Error{
		Remark:     remark,
		StatusCode: statusCode,
	}
}

// NewUnauthorizedError create new UnauthorizedError instance
func NewUnauthorizedError() *Error {
	return NewError("Unauthorized", http.StatusUnauthorized)
}

// NewForbiddenError create new ForbiddenError instance
func NewForbiddenError() *Error {
	return NewError("Forbidden", http.StatusForbidden)
}

// NewBadRequestError create new BadRequestError instance
func NewBadRequestError(message string) *Error {
	return NewError(message, http.StatusBadRequest)
}
