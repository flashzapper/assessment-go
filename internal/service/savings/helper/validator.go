package helper

import (
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

// Define a custom validator with the ability to handle struct tags
type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func NewValidator() echo.Validator {
	return &CustomValidator{
		validator: validator.New(),
	}
}
