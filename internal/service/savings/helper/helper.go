package helper

import (
	conn "assessment-go/pkg/repository/postgres/connection"

	"github.com/go-pg/pg/v10"
)

// GetTransaction get postgre transaction
func GetTransaction() (conn.Transaction, error) {
	var (
		db  *pg.DB
		trx conn.Transaction
		err error
	)

	db = conn.GetConnection("write", "assignment")
	if trx, err = conn.NewTransaction(db); err != nil {
		return nil, err
	}

	return trx, nil
}
