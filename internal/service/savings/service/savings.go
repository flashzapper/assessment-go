package service

import (
	"assessment-go/internal/service/savings/helper"
	"assessment-go/internal/service/savings/model"
	pgModel "assessment-go/pkg/model"
	"assessment-go/pkg/reference"
	conn "assessment-go/pkg/repository/postgres/connection"
	impl "assessment-go/pkg/repository/postgres/impl"
	postgres "assessment-go/pkg/repository/postgres/intfc"
	"encoding/json"
	"errors"
	"log"
	"time"

	k "assessment-go/pkg/modules/kafka"
	kafkaModel "assessment-go/pkg/modules/kafka/model"

	"github.com/go-pg/pg/v10"
)

type SavingsService interface {
	Register(request model.RegisterRequest) (*pgModel.User, *pgModel.Account, error)
	Deposit(request model.DepositRequest) (*pgModel.Account, error)
	Withdraw(request model.WithdrawRequest) (*pgModel.Account, error)
	Balance(request model.BalanceRequest) (*pgModel.Account, error)
	Mutation(request model.MutationRequest) ([]pgModel.Mutation, error)
}

type savingsServiceImpl struct {
	savingsRepo      postgres.SavingsRepository
	savingsRepoWrite postgres.SavingsRepositoryWrite
	producer         k.Producer
}

// Register implements SavingsService.
func (s *savingsServiceImpl) Register(request model.RegisterRequest) (*pgModel.User, *pgModel.Account, error) {
	var (
		trx conn.Transaction
		err error
	)

	// check exist or not Account
	isExist, err := s.savingsRepo.CheckAccountExistByNikOrPhoneNumber(request.NIK, request.PhoneNumber)

	if err != nil {
		return nil, nil, err
	}

	if isExist {
		return nil, nil, nil
	}

	if trx, err = helper.GetTransaction(); err != nil {
		return nil, nil, err
	}

	defer func() {
		if errDefer := trx.RollbackIfNotCommitted(); errDefer != nil {
			log.Printf(errDefer.Error())
		}
	}()

	// insert new User
	modelUser := &pgModel.User{
		Name:        request.Name,
		NIK:         request.NIK,
		PhoneNumber: request.PhoneNumber,
	}

	resultUser, err := s.savingsRepoWrite.SaveUser(trx, modelUser)

	if err != nil {
		return nil, nil, err
	}

	// insert new Account
	modelAccount := &pgModel.Account{
		AccountNumber: resultUser.AccountNumber,
	}

	resultAccount, err := s.savingsRepoWrite.SaveAccount(trx, modelAccount)

	if err != nil {
		return nil, nil, err
	}

	if err = trx.Commit(); err != nil {
		return nil, nil, err
	}

	return resultUser, resultAccount, err
}

// Deposit implements SavingsService.
func (s *savingsServiceImpl) Deposit(request model.DepositRequest) (*pgModel.Account, error) {
	var (
		trx conn.Transaction
		err error
	)

	// check exist or not Account
	isExist, err := s.savingsRepo.CheckAccountExistByAccountNumber(request.AccountNumber)

	if err != nil {
		return nil, err
	}

	if !isExist {
		return nil, nil
	}

	if trx, err = helper.GetTransaction(); err != nil {
		return nil, err
	}

	defer func() {
		if errDefer := trx.RollbackIfNotCommitted(); errDefer != nil {
			log.Printf(errDefer.Error())
		}
	}()

	modelAccount := &pgModel.Account{
		AccountNumber: request.AccountNumber,
	}

	resultAccount, err := s.savingsRepoWrite.AdjustBalance(trx, modelAccount, request.Amount, false)

	if err != nil {
		return nil, err
	}

	if err = s.sendKafkaMessage(resultAccount, string(reference.Credit), request.Amount); err != nil {
		return nil, err
	}

	if err = trx.Commit(); err != nil {
		return nil, err
	}

	return resultAccount, err

}

// Withdraw implements SavingsService.
func (s *savingsServiceImpl) Withdraw(request model.WithdrawRequest) (*pgModel.Account, error) {
	var (
		trx conn.Transaction
		err error
	)

	// check exist or not Account
	existingAccount, err := s.savingsRepo.GetAccountByAccountNumber(request.AccountNumber)

	if err != nil {
		switch err {
		case pg.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	if existingAccount.Balance < request.Amount {
		return nil, errors.New("notenoughbalance")
	}

	if trx, err = helper.GetTransaction(); err != nil {
		return nil, err
	}

	defer func() {
		if errDefer := trx.RollbackIfNotCommitted(); errDefer != nil {
			log.Printf(errDefer.Error())
		}
	}()

	modelAccount := &pgModel.Account{
		AccountNumber: request.AccountNumber,
	}

	resultAccount, err := s.savingsRepoWrite.AdjustBalance(trx, modelAccount, request.Amount, true)

	if err != nil {
		return nil, err
	}

	if err = s.sendKafkaMessage(resultAccount, string(reference.Debit), request.Amount); err != nil {
		return nil, err
	}

	if err = trx.Commit(); err != nil {
		return nil, err
	}

	return resultAccount, err
}

// Balance implements SavingsService.
func (s *savingsServiceImpl) Balance(request model.BalanceRequest) (*pgModel.Account, error) {
	var (
		err error
	)

	// get existing Account
	resultAccount, err := s.savingsRepo.GetAccountByAccountNumber(request.AccountNumber)

	if err != nil {
		switch err {
		case pg.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	return resultAccount, err
}

// Mutation implements SavingsService.
func (s *savingsServiceImpl) Mutation(request model.MutationRequest) ([]pgModel.Mutation, error) {
	var (
		err error
	)

	// get existing Mutations
	resultMutations, err := s.savingsRepo.GetMutationByAccountNumber(request.AccountNumber)

	if err != nil {
		switch err {
		case pg.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	return resultMutations, err
}

func (s *savingsServiceImpl) sendKafkaMessage(model *pgModel.Account, code string, amount float64) error {
	message := kafkaModel.MutationMessage{
		Code:          code,
		AccountNumber: model.AccountNumber,
		Amount:        amount,
		Timestamp:     time.Now().Unix(),
	}

	bMessage, err := json.Marshal(message)

	s.producer.SendMessage(string(k.Mutation), []string{string(bMessage)})
	return err
}

func NewSavingsService() SavingsService {
	return &savingsServiceImpl{
		savingsRepo:      impl.NewSavingsRepository(conn.GetConnection()),
		savingsRepoWrite: impl.NewSavingsRepositoryWrite(),
		producer:         k.NewProducer(),
	}
}
