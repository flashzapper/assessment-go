package controller

import (
	"assessment-go/internal/service/savings/helper"
	"assessment-go/internal/service/savings/model"
	"assessment-go/internal/service/savings/service"
	"assessment-go/pkg/reference"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
)

// SavingsInterface ...
type SavingsController interface {
	Register(c echo.Context) error
	Deposit(c echo.Context) error
	Withdraw(c echo.Context) error
	Balance(c echo.Context) error
	Mutation(c echo.Context) error
}

type savingsImpl struct {
	savingsService service.SavingsService
}

// Register implements SavingsInterface.
func (s *savingsImpl) Register(c echo.Context) error {

	var request model.RegisterRequest
	if err := c.Bind(&request); err != nil {
		return c.JSON(http.StatusBadRequest, helper.NewBadRequestError(err.Error()))
	}

	if err := c.Validate(&request); err != nil {
		return c.JSON(http.StatusBadRequest, helper.NewBadRequestError("Bad Request"))
	}

	svcResponse, _, err := s.savingsService.Register(request)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, helper.NewError(err.Error(), http.StatusInternalServerError))
	}

	if svcResponse == nil {
		return c.JSON(http.StatusBadRequest, helper.NewBadRequestError("No HP atau NIK sudah terdaftar"))
	}

	// Serialize the service response
	response := model.RegisterResponse{
		AccountNumber: svcResponse.AccountNumber,
	}

	return c.JSON(http.StatusOK, model.NewSuccessResponse(response))
}

// Save implements SavingsInterface.
func (s *savingsImpl) Deposit(c echo.Context) error {
	var request model.DepositRequest
	if err := c.Bind(&request); err != nil {
		return c.JSON(http.StatusBadRequest, helper.NewBadRequestError(err.Error()))
	}

	if err := c.Validate(&request); err != nil {
		return c.JSON(http.StatusBadRequest, helper.NewBadRequestError("Bad Request"))
	}

	svcResponse, err := s.savingsService.Deposit(request)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, helper.NewError(err.Error(), http.StatusInternalServerError))
	}

	if svcResponse == nil {
		return c.JSON(http.StatusBadRequest, helper.NewBadRequestError("No Rekening tidak terdaftar"))
	}

	// Serialize the service response
	response := model.DepositResponse{
		Balance: svcResponse.Balance,
	}

	return c.JSON(http.StatusOK, model.NewSuccessResponse(response))
}

// Withdraw implements SavingsInterface.
func (s *savingsImpl) Withdraw(c echo.Context) error {
	var request model.WithdrawRequest
	if err := c.Bind(&request); err != nil {
		return c.JSON(http.StatusBadRequest, helper.NewBadRequestError(err.Error()))
	}

	if err := c.Validate(&request); err != nil {
		return c.JSON(http.StatusBadRequest, helper.NewBadRequestError("Bad Request"))
	}

	svcResponse, err := s.savingsService.Withdraw(request)

	if err != nil {
		switch err.Error() {
		case "notenoughbalance":
			return c.JSON(http.StatusBadRequest, helper.NewBadRequestError("Saldo tidak cukup"))
		default:
			return c.JSON(http.StatusInternalServerError, helper.NewError(err.Error(), http.StatusInternalServerError))
		}
	}

	if svcResponse == nil {
		return c.JSON(http.StatusBadRequest, helper.NewBadRequestError("No Rekening tidak terdaftar"))
	}

	// Serialize the service response
	response := model.WithdrawResponse{
		Balance: svcResponse.Balance,
	}

	return c.JSON(http.StatusOK, model.NewSuccessResponse(response))
}

// Balance implements SavingsInterface.
func (s *savingsImpl) Balance(c echo.Context) error {
	var request model.BalanceRequest

	request.AccountNumber = c.Param("no_rekening")

	if err := c.Validate(&request); err != nil {
		return c.JSON(http.StatusBadRequest, helper.NewBadRequestError("Bad Request"))
	}

	svcResponse, err := s.savingsService.Balance(request)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, helper.NewError(err.Error(), http.StatusInternalServerError))
	}

	if svcResponse == nil {
		return c.JSON(http.StatusBadRequest, helper.NewBadRequestError("No Rekening tidak terdaftar"))
	}

	// Serialize the service response
	response := model.BalanceResponse{
		Balance: svcResponse.Balance,
	}

	return c.JSON(http.StatusOK, model.NewSuccessResponse(response))
}

// Mutation implements SavingsInterface.
func (s *savingsImpl) Mutation(c echo.Context) error {
	var request model.MutationRequest

	request.AccountNumber = c.Param("no_rekening")

	if err := c.Validate(&request); err != nil {
		return c.JSON(http.StatusBadRequest, helper.NewBadRequestError("Bad Request"))
	}

	svcResponse, err := s.savingsService.Mutation(request)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, helper.NewError(err.Error(), http.StatusInternalServerError))
	}

	if svcResponse == nil {
		return c.JSON(http.StatusBadRequest, helper.NewBadRequestError("No Rekening tidak terdaftar"))
	}

	// Serialize the service response
	var mutations = []model.Mutation{}
	timezone, _ := time.LoadLocation("Asia/Jakarta")
	for _, respMutation := range svcResponse {
		var mutation = model.Mutation{}
		mutation.TransactionCode = reference.TransactionCode(respMutation.Code)
		mutation.Amount = respMutation.Amount
		mutation.Time = respMutation.CreatedTime.In(timezone).Format("2006-01-02 15:04:05")
		mutations = append(mutations, mutation)
	}

	response := model.MutationResponse{
		Mutation: mutations,
	}

	return c.JSON(http.StatusOK, model.NewSuccessResponse(response))
}

// NewSavingsController ...
func NewSavingsController() SavingsController {
	return &savingsImpl{
		savingsService: service.NewSavingsService(),
	}
}
