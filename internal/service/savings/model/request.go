package model

type RegisterRequest struct {
	Name        string `json:"nama" validate:"required"`
	NIK         string `json:"nik" validate:"required,numeric"`
	PhoneNumber string `json:"no_hp" validate:"required,numeric"`
}

type DepositRequest struct {
	AccountNumber string  `json:"no_rekening" validate:"required,numeric"`
	Amount        float64 `json:"nominal" validate:"required,numeric"`
}

type WithdrawRequest struct {
	AccountNumber string  `json:"no_rekening" validate:"required,numeric"`
	Amount        float64 `json:"nominal" validate:"required,numeric"`
}

type BalanceRequest struct {
	AccountNumber string `json:"no_rekening" validate:"required,numeric"`
}

type MutationRequest struct {
	AccountNumber string `json:"no_rekening" validate:"required,numeric"`
}
