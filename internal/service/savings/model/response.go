package model

import "assessment-go/pkg/reference"

// BaseResponse general response purpose
type BaseResponse struct {
	Remark string      `json:"remark"`
	Data   interface{} `json:"data"`
}

type RegisterResponse struct {
	AccountNumber string `json:"no_rekening"`
}

type DepositResponse struct {
	Balance float64 `json:"saldo"`
}

type WithdrawResponse struct {
	Balance float64 `json:"saldo"`
}

type BalanceResponse struct {
	Balance float64 `json:"saldo"`
}

type MutationResponse struct {
	Mutation []Mutation `json:"mutasi"`
}

type Mutation struct {
	Time            string                    `json:"waktu"`
	TransactionCode reference.TransactionCode `json:"kode_transaksi"`
	Amount          float64                   `json:"nominal"`
}

// NewResponse create new BaseResponse instance
func NewResponse(isSuccess bool, remark string, data interface{}) BaseResponse {
	return BaseResponse{
		Remark: remark,
		Data:   data,
	}
}

// NewSuccessResponse create new BaseResponse instance for success case
func NewSuccessResponse(data interface{}) BaseResponse {
	return NewResponse(true, "Success", data)
}
