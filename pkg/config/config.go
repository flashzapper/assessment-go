package config

import (
	"log"
	"os"
	"sync"

	"gopkg.in/ini.v1"
)

const (
	// ConfigErrorCode Error code for configuration.
	ConfigErrorCode = 127
)

var (
	config *ini.File

	// echo-server
	onceEchoServerConfig sync.Once
	echoServerConfig     EchoServerConfig
)

func init() {
	var err error
	configPath := os.Getenv("APP_CONFIG")

	path := os.Getenv("APP_DIR")
	if len(configPath) > 0 {
		path = configPath
	} else if len(path) > 0 {
		path += "/app.ini"
	} else {
		path = "app.ini"
	}

	config, err = ini.LoadSources(ini.LoadOptions{
		Insensitive:                true,
		AllowPythonMultilineValues: true,
	}, path)
	if err != nil {
		log.Println("Configuration file could not be loaded")
		os.Exit(ConfigErrorCode)
	}
}

// EchoServerConfig ...
func GetEchoServerConfig() EchoServerConfig {
	onceEchoServerConfig.Do(func() {
		echoServerConfig = EchoServerConfig{
			Port: config.Section("echo-server").Key("PORT").MustString("5000"),
		}
	})

	return echoServerConfig
}

// GetDatabaseConfig Get Database config
func GetDatabaseConfig(t ...string) map[string]string {
	sectionName := "database"
	if len(t) >= 1 {
		sectionName += "." + t[0]
	}
	sec := config.Section(sectionName)
	if sec.Key("db").String() != "postgres" {
		log.Panic("only support posrgres value")
	}

	return map[string]string{
		"PG_HOST": sec.Key("pg_host").String(),
		"PG_PORT": sec.Key("pg_port").String(),
		"PG_USER": sec.Key("pg_user").String(),
		"PG_PASS": sec.Key("pg_pass").String(),
		"PG_POOL": sec.Key("pg_pool").String(),
		"PG_NAME": sec.Key("pg_name").String(),
	}
}

// GetKafkaConfig Get kafka config.
func GetKafkaConfig(keyType string) string {
	return config.Section("kafka").Key(keyType).String()
}
