package model

import (
	"context"
	"time"

	"github.com/gofrs/uuid"
)

type Mutation struct {
	tableName     struct{}   `pg:"assessment.mutations,discard_unknown_columns"`
	UUID          *uuid.UUID `pg:",pk,type:uuid"` // Primary key field (use UUID type)
	AccountNumber string     `pg:"account_number"`
	Code          string     `pg:"code"`
	Amount        float64    `pg:"amount"`
	CreatedTime   time.Time  `pg:"created_time,notnull"`
	UpdatedTime   *time.Time `pg:"updated_time"`
	IsRemoved     bool       `pg:"removed_time"`
	RemovedTime   *time.Time `pg:"removed_time"`
}

// BeforeInsert hooks to set default uuid & created time
func (b *Mutation) BeforeInsert(ctx context.Context) (context.Context, error) {
	if b.UUID == nil {
		newUUID, _ := uuid.NewV4()
		b.UUID = &newUUID
	}

	b.CreatedTime = time.Now()

	return ctx, nil
}
