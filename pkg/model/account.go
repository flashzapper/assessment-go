package model

import (
	"context"
	"time"

	"github.com/gofrs/uuid"
)

type Account struct {
	tableName     struct{}   `pg:"assessment.accounts,discard_unknown_columns"`
	UUID          *uuid.UUID `pg:",pk,type:uuid"` // Primary key field (use UUID type)
	AccountNumber string     `pg:"account_number"`
	Balance       float64    `pg:"balance"`
	Status        int        `pg:"status"`
	CreatedTime   time.Time  `pg:"created_time,notnull"`
	UpdatedTime   *time.Time `pg:"updated_time"`
	IsRemoved     bool       `pg:"removed_time"`
	RemovedTime   *time.Time `pg:"removed_time"`
}

// BeforeInsert hooks to set default uuid & created time
func (b *Account) BeforeInsert(ctx context.Context) (context.Context, error) {
	if b.UUID == nil {
		newUUID, _ := uuid.NewV4()
		b.UUID = &newUUID
	}

	b.CreatedTime = time.Now()

	return ctx, nil
}
