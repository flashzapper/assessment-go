package model

import (
	"context"
	"fmt"
	"time"

	"github.com/gofrs/uuid"
)

type User struct {
	tableName     struct{}   `pg:"assessment.users,discard_unknown_columns"`
	UUID          *uuid.UUID `pg:",pk,type:uuid"` // Primary key field (use UUID type)
	Name          string     `pg:"name"`
	NIK           string     `pg:"nik"`
	AccountNumber string     `pg:"account_number"`
	PhoneNumber   string     `pg:"phone_number"`
	Status        int        `pg:"status"`
	CreatedTime   time.Time  `pg:"created_time,notnull"`
	UpdatedTime   *time.Time `pg:"updated_time"`
	IsRemoved     bool       `pg:"removed_time"`
	RemovedTime   *time.Time `pg:"removed_time"`
}

// BeforeInsert hooks to set default uuid & created time
func (b *User) BeforeInsert(ctx context.Context) (context.Context, error) {
	if b.UUID == nil {
		newUUID, _ := uuid.NewV4()
		b.UUID = &newUUID
	}

	if b.AccountNumber == "" {
		b.AccountNumber = fmt.Sprintf("%d%s", time.Now().Unix(), b.PhoneNumber)
	}

	b.CreatedTime = time.Now()

	return ctx, nil
}
