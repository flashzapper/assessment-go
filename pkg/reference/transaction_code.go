package reference

type TransactionCode string

const (
	Credit TransactionCode = "C"
	Debit  TransactionCode = "D"
)

func (t TransactionCode) Translate() string {
	switch t {
	case Debit:
		return "Debit"
	default:
		return "Credit"
	}
}
