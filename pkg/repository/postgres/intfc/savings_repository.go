package postgres

import (
	"assessment-go/pkg/model"
	postgres "assessment-go/pkg/repository/postgres/connection"
)

type SavingsRepository interface {
	CheckAccountExistByNikOrPhoneNumber(nik string, phoneNumber string) (bool, error)
	CheckAccountExistByAccountNumber(accountNumber string) (bool, error)
	GetAccountByAccountNumber(accountNumber string) (*model.Account, error)
	GetMutationByAccountNumber(accountNumber string) ([]model.Mutation, error)
}

type SavingsRepositoryWrite interface {
	SaveUser(trx postgres.Transaction, model *model.User) (*model.User, error)
	SaveAccount(trx postgres.Transaction, model *model.Account) (*model.Account, error)
	AdjustBalance(trx postgres.Transaction, model *model.Account, amount float64, isDebit bool) (*model.Account, error)
	SaveMutation(trx postgres.Transaction, model *model.Mutation) (*model.Mutation, error)
}
