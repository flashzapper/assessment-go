package postgres

import "github.com/go-pg/pg/v10"

// Transaction ...
type Transaction interface {
	GetTransaction() *pg.Tx
	Commit() error
	Rollback() error
	RollbackIfNotCommitted() error
}
