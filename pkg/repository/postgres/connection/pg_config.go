package postgres

import (
	"assessment-go/pkg/config"
	"context"
	"strconv"
	"sync"

	"log"

	"github.com/go-pg/pg/v10"
)

const (
	defaultConn        string = "pgDefault"
	defaultSearchPath  string = "public"
	defaultPoolSize    int    = 5
	defaultMinIdleConn int    = 2
	defaultRetry       int    = 3
)

var (
	iMapPostgre          = make(map[string]*pg.DB)
	iMapSingletonPostgre = make(map[string]*sync.Once)
	mapMutex             = sync.RWMutex{}
)

// GetConnection returns database connection.
func GetConnection(cn ...string) *pg.DB {
	var connName = defaultConn
	var searchPath = defaultSearchPath

	if len(cn) >= 2 {
		connName = cn[0]
		searchPath = cn[1]
	}

	_cn := connName + "-" + searchPath

	mapMutex.Lock()
	defer mapMutex.Unlock()
	if _, ok := iMapSingletonPostgre[_cn]; !ok {
		iMapSingletonPostgre[_cn] = new(sync.Once)
	} else {
		if err := iMapPostgre[_cn].Ping(iMapPostgre[_cn].Context()); err != nil {
			if err := iMapPostgre[_cn].Close(); err != nil {
				log.Printf(err.Error())
			}
			iMapSingletonPostgre[_cn] = new(sync.Once)
		}
	}

	(*iMapSingletonPostgre[_cn]).Do(func() {
		var conf map[string]string

		conf = config.GetDatabaseConfig()

		poolSize, poolErr := strconv.Atoi(conf["PG_POOL"])
		minIdleConn, idleErr := strconv.Atoi(conf["PG_MIN_IDLE"])

		if poolErr != nil {
			poolSize = defaultPoolSize
		}

		if idleErr != nil {
			minIdleConn = defaultMinIdleConn
		}

		iMapPostgre[_cn] = pgConnect(conf, poolSize, minIdleConn, searchPath)
	})

	conn := iMapPostgre[_cn]
	return conn
}

func pgConnect(conf map[string]string, poolSize int, minIdleConn int, searchPath string) *pg.DB {
	return pg.Connect(&pg.Options{
		Addr:         conf["PG_HOST"] + ":" + conf["PG_PORT"],
		User:         conf["PG_USER"],
		Password:     conf["PG_PASS"],
		Database:     conf["PG_NAME"],
		PoolSize:     defaultPoolSize,
		MinIdleConns: defaultMinIdleConn,
		MaxRetries:   defaultRetry,
		OnConnect: func(ctx context.Context, conn *pg.Conn) error {
			_, err := conn.Exec("set search_path=?", searchPath)
			if err != nil {
				log.Printf(err.Error())
			}
			return nil
		},
	})
}
