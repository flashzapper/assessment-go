package postgres

import (
	"errors"

	"github.com/go-pg/pg/v10"
)

var (
	ok                  = true
	constNilTransaction = "nil transaction"
)

type transactionImpl struct {
	tx          *pg.Tx
	isCommitted *bool
}

func (t *transactionImpl) GetTransaction() *pg.Tx {
	return t.tx
}

func (t *transactionImpl) Commit() error {
	if t.tx == nil {
		return errors.New(constNilTransaction)
	}

	if err := t.tx.Commit(); err != nil {
		return err
	}

	t.isCommitted = &ok

	return nil
}

func (t *transactionImpl) Rollback() error {
	if t.tx == nil {
		return errors.New(constNilTransaction)
	}

	return t.tx.Rollback()
}

func (t *transactionImpl) RollbackIfNotCommitted() error {
	if t.isCommitted != nil && *t.isCommitted {
		return nil
	}

	return t.Rollback()
}

// NewTransaction ...
func NewTransaction(db *pg.DB) (Transaction, error) {
	if db == nil {
		return nil, errors.New("nil db provided")
	}

	tx, err := db.Begin()
	if err != nil {
		return nil, err
	}

	return &transactionImpl{tx: tx}, nil
}
