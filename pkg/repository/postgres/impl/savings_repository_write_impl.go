package postgres

import (
	"assessment-go/pkg/model"
	conn "assessment-go/pkg/repository/postgres/connection"
	postgres "assessment-go/pkg/repository/postgres/intfc"
	"fmt"
)

type savingsRepositoryWriteImpl struct {
}

// SaveUser implements postgres.SavingsRepositoryWrite.
func (s *savingsRepositoryWriteImpl) SaveUser(trx conn.Transaction, model *model.User) (*model.User, error) {
	_, err := trx.GetTransaction().
		Model(model).
		Returning("*").
		Insert()

	return model, err
}

// SaveAccount implements postgres.SavingsRepositoryWrite.
func (s *savingsRepositoryWriteImpl) SaveAccount(trx conn.Transaction, model *model.Account) (*model.Account, error) {
	_, err := trx.GetTransaction().
		Model(model).
		Returning("*").
		Insert()

	return model, err
}

// AdjustBalance implements postgres.SavingsRepositoryWrite.
func (s *savingsRepositoryWriteImpl) AdjustBalance(trx conn.Transaction, model *model.Account, amount float64, isDebit bool) (*model.Account, error) {
	var (
		operator     string
		setStatement string
	)

	operator = "+"
	if isDebit {
		operator = "-"
	}

	setStatement = fmt.Sprintf("balance = balance %s %f", operator, amount)

	_, err := trx.GetTransaction().
		Model(model).
		Where("account_number = ?", model.AccountNumber).
		Set(setStatement).
		Set("updated_time = now()").
		Returning("*").
		Update()

	return model, err
}

// SaveMutation implements postgres.SavingsRepositoryWrite.
func (s *savingsRepositoryWriteImpl) SaveMutation(trx conn.Transaction, model *model.Mutation) (*model.Mutation, error) {
	_, err := trx.GetTransaction().
		Model(model).
		Returning("*").
		Insert()

	return model, err
}

// NewSavingsRepositoryWrite returns savings repository for write based on db drive
func NewSavingsRepositoryWrite() postgres.SavingsRepositoryWrite {
	return &savingsRepositoryWriteImpl{}
}
