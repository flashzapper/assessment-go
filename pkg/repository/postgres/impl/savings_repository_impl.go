package postgres

import (
	"assessment-go/pkg/model"
	postgres "assessment-go/pkg/repository/postgres/intfc"
	"log"

	"github.com/go-pg/pg/v10"
)

type savingsRepositoryImpl struct {
	db *pg.DB
}

// CheckAccountExistByNikOrPhoneNumber implements postgres.SavingsRepository.
func (t *savingsRepositoryImpl) CheckAccountExistByNikOrPhoneNumber(nik string, phoneNumber string) (bool, error) {
	query := t.db.Model(&model.User{})
	query.WhereOr("nik = ?", nik)
	query.WhereOr("phone_number = ?", phoneNumber)

	exists, err := query.Exists()

	if err != nil {
		log.Print(err)
		return false, err
	}

	return exists, nil
}

// CheckAccountExistByAccountNumber implements postgres.SavingsRepository.
func (s *savingsRepositoryImpl) CheckAccountExistByAccountNumber(accountNumber string) (bool, error) {
	query := s.db.Model(&model.User{})
	query.Where("account_number = ?", accountNumber)

	exists, err := query.Exists()

	if err != nil {
		log.Print(err)
		return false, err
	}

	return exists, nil
}

// GetAccountByAccountNumber implements postgres.SavingsRepository.
func (s *savingsRepositoryImpl) GetAccountByAccountNumber(accountNumber string) (*model.Account, error) {
	var result model.Account
	result = model.Account{}
	err := s.db.Model(&result).
		Where("account_number = ?", accountNumber).
		First()

	if err != nil {
		log.Print(err)
		return nil, err
	}

	return &result, nil
}

// GetMutationByAccountNumber implements postgres.SavingsRepository.
func (s *savingsRepositoryImpl) GetMutationByAccountNumber(accountNumber string) ([]model.Mutation, error) {
	var result []model.Mutation
	err := s.db.Model(&result).
		Where("account_number = ?", accountNumber).
		Select()

	if err != nil {
		log.Print(err)
		return nil, err
	}

	return result, nil
}

// NewSavingsRepository returns savings repository for read based on db drive
func NewSavingsRepository(db *pg.DB) postgres.SavingsRepository {
	return &savingsRepositoryImpl{
		db: db,
	}
}
