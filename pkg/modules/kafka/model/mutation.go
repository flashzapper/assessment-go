package model

import (
	"encoding/json"

	"github.com/gofrs/uuid"
)

// MutationMessage message for mutation kafka
type MutationMessage struct {
	MessageID     uuid.UUID
	Message       string
	Code          string
	AccountNumber string
	Amount        float64
	Timestamp     int64
}

// FromKafka will convert message from kafka to MutationMessage
func (m MutationMessage) FromKafka(value []byte) MutationMessage {
	var data MutationMessage
	if err := json.Unmarshal(value, &data); err != nil {
		return data
	}
	return data
}
