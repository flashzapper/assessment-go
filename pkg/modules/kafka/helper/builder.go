package helper

import (
	"assessment-go/pkg/modules/kafka/model"
	"encoding/json"
	"fmt"
)

// BuildMessageError Build Message Error
func BuildMessageError(data interface{}, message string) string {
	var bMessage []byte
	var err error

	if sData, ok := data.(model.MutationMessage); ok {
		sData.Message = message
		bMessage, err = json.Marshal(sData)
	}

	if err != nil {
		fmt.Printf("Error: %s", err)
		return ""
	}
	return string(bMessage)
}
