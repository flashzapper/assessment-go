package kafka

import (
	"assessment-go/pkg/config"
	"fmt"
	"math"

	kafkaclient "github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/labstack/gommon/log"
)

// Producer is kafka producer struct
type Producer struct {
	p *kafkaclient.Producer
}

// NewProducer is create kafka client
func NewProducer() Producer {
	p, err := kafkaclient.NewProducer(&kafkaclient.ConfigMap{
		"bootstrap.servers": config.GetKafkaConfig("server"),
	})
	if err != nil {
		panic(err)
	}
	publisherReport(p)
	return Producer{p}
}

// SendMessage Produce messages to topic (asynchronously)
func (p Producer) SendMessage(topic string, messages []string) {
	pr := p.getProducer()
	total := len(messages)
	chunk := 100
	for i := 0; i < len(messages); i += chunk {
		batch := messages[i:int(math.Min(float64(i+chunk), float64(total)))]
		log.Infof("produce %d of %d to %s", i+len(batch), total, topic)
		for _, word := range batch {
			err := pr.Produce(&kafkaclient.Message{
				TopicPartition: kafkaclient.TopicPartition{Topic: &topic, Partition: kafkaclient.PartitionAny},
				Value:          []byte(word),
			}, nil)
			if err != nil {
				log.Error(err)
			}
		}
		// Wait for message deliveries before shutting down
		pr.Flush(15 * 1000)
	}
}

func (p Producer) getProducer() *kafkaclient.Producer {
	if p.p != nil {
		return p.p
	}
	pr, err := kafkaclient.NewProducer(&kafkaclient.ConfigMap{
		"bootstrap.servers": config.GetKafkaConfig("server"),
	})
	if err != nil {
		panic(err)
	}
	p.p = pr
	return p.p
}

// Delivery report handler for produced messages
func publisherReport(p *kafkaclient.Producer) {
	go func() {
		for e := range p.Events() {
			switch ev := e.(type) {
			case *kafkaclient.Message:
				if ev.TopicPartition.Error != nil {
					fmt.Printf("Delivery failed: %v\n", ev.TopicPartition)
				}
			}
		}
	}()
}
