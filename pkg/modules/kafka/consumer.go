package kafka

import (
	"assessment-go/pkg/config"

	kafkaclient "github.com/confluentinc/confluent-kafka-go/kafka"
)

// ConsumerArgs is kafkaclient consumer args struct
type ConsumerArgs struct {
	GroupID    *string
	Server     *string
	Offset     *string
	AutoCommit *bool
}

// Consumer is kafkaclient consumer struct
type Consumer struct {
	c    *kafkaclient.Consumer
	args *ConsumerArgs
}

// NewConsumer is create kafkaclient client
func NewConsumer(args *ConsumerArgs) Consumer {
	c, err := kafkaclient.NewConsumer(getConfigMap(args))
	if err != nil {
		panic(err)
	}
	return Consumer{c, args}
}

// SubscribeTopic for subscribe 1 topic
func (c Consumer) SubscribeTopic(topic string) {
	_ = c.getConsumer().SubscribeTopics([]string{topic}, nil)
}

// GetMessage for get message from kafkaclient
func (c Consumer) GetMessage() (*kafkaclient.Message, error) {
	msg, err := c.getConsumer().ReadMessage(-1)
	return msg, err
}

// Poll for get event from kafkaclient
func (c Consumer) Poll(timeoutMs int) kafkaclient.Event {
	return c.getConsumer().Poll(timeoutMs)
}

// Commit for commit offset to kafkaclient
func (c Consumer) Commit() ([]kafkaclient.TopicPartition, error) {
	return c.getConsumer().Commit()
}

func (c Consumer) getConsumer() *kafkaclient.Consumer {
	if c.c != nil {
		return c.c
	}
	cr, err := kafkaclient.NewConsumer(getConfigMap(c.args))
	if err != nil {
		panic(err)
	}
	c.c = cr
	return c.c
}

// getConfigMap for get config map kafkaclient, default get config from file app.ini
func getConfigMap(args *ConsumerArgs) *kafkaclient.ConfigMap {
	configMap := kafkaclient.ConfigMap{
		"bootstrap.servers":  config.GetKafkaConfig("server"),
		"group.id":           config.GetKafkaConfig("groupId"),
		"auto.offset.reset":  config.GetKafkaConfig("offset"),
		"enable.auto.commit": true,
	}

	if args == nil {
		return &configMap
	}

	if args.GroupID != nil {
		configMap["group.id"] = *args.GroupID
	}

	if args.Offset != nil {
		configMap["auto.offset.reset"] = *args.Offset
	}

	if args.Server != nil {
		configMap["bootstrap.servers"] = *args.Server
	}

	if args.AutoCommit != nil {
		configMap["enable.auto.commit"] = *args.AutoCommit
	}

	return &configMap
}
